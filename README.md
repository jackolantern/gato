# GrAphical Terminal Output

Outputs images in a KiTTY terminal: https://sw.kovidgoyal.net/kitty/

Gato differs from KiTTY's builtin `icat` command in that it can display
animated gifs and it can "emojify" text.

Example usage:

To display an animated gif:
```bash
gato --gif ~/img/animated/ed.3.gif
```
![Screen Shot](https://gitlab.com/jackolantern/gato/uploads/c86321ff2c5b8f1cf9690166a9394d83/gato-2018-10-20_15.00.24.gif)

To emojify text:
```bash
gato --emojify - <<< "The worst situation: :no_mouth: :beer:"
```
![Screen Shot](https://gitlab.com/jackolantern/gato/uploads/7543f83ae0a120ca93f0ea0a4a6ac9ba/DeepinScreenshot_select-area_20181021103200.png)

By default, the `--emojify` option causes `gato` to look for files in the
OS specific application data directory.  On Arch Linux this is evidently
`$HOME/.local/share/gato/emoji`.  Any 'png' format files in that directory
which have a "file extension" of `.png` will be found and displayed by `gato`.
A different directory can be specified by using the `--path` option.

